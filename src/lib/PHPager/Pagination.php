<?php


namespace PHPager;


class Pagination {

    private int $count;

    private int $totalPages;

    private int $currentPageNumber;

    // a array with items of the page
    private array $page;

    private array $collection;

    private int $limit;

    function __construct(
        array $collection,
        ?int $limit = null
        ) {
            $this->collection = $collection;
            $this->count = count($this->collection);
            $this->limit = $this->resolveLimit($limit);
            $this->totalPages = self::calculateTotalPages($this->count, $this->limit);
            $this->page = [];
            $this->currentPageNumber = 1;
    }

    static function calculateTotalPages(int $count, ?int $limit = null): int {

        if (!is_null($limit) && $limit > 0 && $count > 0) {
            return ceil($count / $limit);
        }

        return 1;
    }

    static function calculateOffset(int $count, int $limit, int $pageNumber = 1): int {

        $totalPages = self::calculateTotalPages($count, $limit);

        $pageNumber = self::resolvePageNumber($pageNumber, $totalPages);

        $offset = $limit * ($pageNumber - 1);

        if ($offset < 0) {
            $offset = 0;
        }

        return $offset;
    }

    function setLimit(int $value) {
        $this->limit = $value;
    }

    function getPage(int $pageNumber): array {

        if ((count($this->collection) && !count($this->page))
            || ($pageNumber !== $this->currentPageNumber)
            )
        {
            $this->currentPageNumber = self::resolvePageNumber($pageNumber, $this->totalPages);
            $offset = self::calculateOffset($this->count, $this->limit, $this->currentPageNumber);
            $this->page = array_slice($this->collection, $offset, $this->limit);
        }

        return $this->page;
    }

    private static function resolvePageNumber(int $pageNumber = 1, int $totalPages = 1): int {

        if ($totalPages < 1) {
            $totalPages = 1;
        }

        if ($pageNumber < 1) {
            $pageNumber = 1;
        } else if ($pageNumber > $totalPages) {
            $pageNumber = $totalPages;
        }

        return $pageNumber;
    }

    private function resolveLimit(?int $limit = null): int {

        if ($this->count === 0 || (!is_null($limit) && $limit < 0)) {
            return 0;
        } else if (is_null($limit)) {
            return count($this->collection);
        }

        return $limit;
    }

}
