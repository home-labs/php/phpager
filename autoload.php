<?php


function psr4Autoload($className) {
    include str_replace('\\', DIRECTORY_SEPARATOR, $className)  . '.php';
}


$rootPath = realpath(__DIR__);

$settingsPathOfTests =  implode(DIRECTORY_SEPARATOR, [$rootPath, 'src', 'settings']);

$pathIncluder = implode(DIRECTORY_SEPARATOR, [$settingsPathOfTests, 'path4autoload-includer.php']);


require_once $pathIncluder;


spl_autoload_register('psr4Autoload');
